import { BaseModel } from './Generic/BaseModel'
export class IdiomaModel extends BaseModel {
  nombre: string;
  codigo: string;
  constructor() {
    super()
    this.nombre = null;
    this.codigo = null;
  }

}
