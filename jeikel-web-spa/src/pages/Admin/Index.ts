export { CompetenciasMaintenance } from './CompetenciasMaintenance/Index'
export { DepartamentoMaintenance } from './DepartamentoMaintenance/Index'
export { IdiomaMaintenance } from './IdiomaMaintenance/index'
export { InstitucionMaintenance } from './InstitucionMaintenance/Index'
export { PosicionesTrabajoMaintenance } from './PosicionesTrabajoMaintenance/Index'
export { TipoIdentificationMaintenance } from './TipoIdentificationMaintenance/Index'

