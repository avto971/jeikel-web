import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'

@Component({
  template: require('./DepartamentoMaintenance.html'),
})
export class DepartamentoMaintenance extends GenericPage<IdiomaModel> {

  constructor() {
    super('', {
      Nombre: {
        label: 'Nombre',
        sortable: true
      },
      Codigo: {
        label: 'Descripcion',
        sortable: false
      }
    }, IdiomaModel)
  }

}



