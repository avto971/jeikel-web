
import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'
@Component({
  template: require('./InstitucionMaintenance.html'),
})
export class InstitucionMaintenance extends GenericPage<IdiomaModel> {


  constructor() {
    super('', {
      Nombre: {
        label: 'Nombre',
        sortable: true
      },
      Codigo: {
        label: 'Descripción',
        sortable: false
      },
      Adicional: {
        key: 'Adicional.Numeracion',
        label: 'Informacion Adicional',
        sortable: true
        ,
      },
    }, IdiomaModel)
  }


}
