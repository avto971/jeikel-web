import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'


@Component({
  template: require('./TipoIdentificationMaintenance.html'),
})
export class TipoIdentificationMaintenance extends GenericPage<IdiomaModel> {


  constructor() {
    super('', {
      Nombre: {
        label: 'Nombre',
        sortable: true
      }
    }, IdiomaModel)
  }

}
