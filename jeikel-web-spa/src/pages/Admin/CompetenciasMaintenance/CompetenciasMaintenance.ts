import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'

@Component({
  template: require('./CompetenciasMaintenance.html'),
})
export class CompetenciasMaintenance extends GenericPage<IdiomaModel> {

  constructor() {
    super('', {
      Nombre: {
        label: 'Nombre',
        sortable: true
      },
      Description: {
        label: 'Descripcion',
        sortable: false
      },
    }, IdiomaModel)
  }

}



