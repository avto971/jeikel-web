import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'

@Component({
  template: require('./IdiomaMaintenance.html'),
})
export class IdiomaMaintenance extends GenericPage<IdiomaModel> {

  constructor() {
    super('', {
        Nombre: {
          label: 'Nombre',
          sortable: true
        },
        Codigo: {
          label: 'Código',
          sortable: false
        },
        Adicional: {
          key: 'Adicional.Numeracion',
          label: 'Informacion Adicional',
          sortable: true
          ,
        },
    }, IdiomaModel)
    }

  }



