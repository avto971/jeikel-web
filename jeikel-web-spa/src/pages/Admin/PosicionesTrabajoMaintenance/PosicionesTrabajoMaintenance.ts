
import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'
@Component({
  template: require('./PosicionesTrabajoMaintenance.html'),
})
export class PosicionesTrabajoMaintenance extends GenericPage<IdiomaModel> {


  constructor() {
    super('', {
      Nombre: {
        label: 'Nombre',
        sortable: true
      },
      Riesgo: {
        label: 'Riesgo',
        sortable: false
      },
      SalarioMinimo: {
        key: 'Adicional.Numeracion',
        label: 'Rango Mínimo',
        sortable: true
        ,
      },
      SalarioMaximo: {
        key: 'Adicional.Numeracion',
        label: 'Rango Máximo',
        sortable: true,
      }
    }, IdiomaModel)
  }


}
