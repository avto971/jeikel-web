import { Component, Vue } from 'vue-property-decorator'
import StaticMap from 'vue-static-map';
@Component({
  template: require('./mapaPage.html'),
  components: { StaticMap}
  })
export class MapaPage extends Vue{

  apiKey = 'AIzaSyA4NU3iWktanOclzWGZZpqz7KKcUHWrF7k';
  zoom = 8;
  center = 'Dominican+Republic La+Espanola';
  markers = [
    {
      label: 'B',
      color: 'blue',
      lat: 18.755603,
      lng: -70.8109277,
      size: 'normal',
    }
   
     
    //{
    //  label: 'Y',
    //  color: 'yellow',
    //  lat: 40.711614,
    //  lng: -74.012318,
    //  size: 'tiny',
    //},
    //{
    //  label: 'G',
    //  color: 'green',
    //  lat: 40.718217,
    //  lng: -74.015794,
    //  size: 'small',
    //  icon: 'http://www.airsoftmap.net/images/pin_map.png',
    //}
  ];

  size = [1024, 1024]
  
}   
