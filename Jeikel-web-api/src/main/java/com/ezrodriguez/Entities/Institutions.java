package com.ezrodriguez.Entities;

public class Institutions {
    private int Id;
    private String Name;
    private String Description;
    private boolean Deleted;
    private int CreatedBy;
    private int UpdatedBy;

    public Institutions(int id, String name, String description, boolean deleted, int createdBy, int updatedBy) {
        Id = id;
        Name = name;
        Description = description;
        Deleted = deleted;
        CreatedBy = createdBy;
        UpdatedBy = updatedBy;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public boolean isDeleted() {
        return Deleted;
    }

    public void setDeleted(boolean deleted) {
        Deleted = deleted;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public int getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        UpdatedBy = updatedBy;
    }
}
