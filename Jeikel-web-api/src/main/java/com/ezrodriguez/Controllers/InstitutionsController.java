package com.ezrodriguez.Controllers;

import com.ezrodriguez.Entities.Institutions;
import com.ezrodriguez.Services.InstitutionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8080"}, allowedHeaders = "true")
@RestController
@RequestMapping("api/Institutions")
public class InstitutionsController {

    @Autowired
    private InstitutionsService institutionsService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Institutions> getAllProducts(){
        return institutionsService.getAllInstitutions();
    }


    @RequestMapping(method = RequestMethod.GET, path = "/writeExcel")
    public String writeExcel(){
        return institutionsService.Write();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/readExcel")
    public String readExcel(){
        return institutionsService.Read();
    }
}
