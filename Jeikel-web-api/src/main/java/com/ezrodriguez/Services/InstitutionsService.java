package com.ezrodriguez.Services;

import com.ezrodriguez.Dao.InstitutionsDao;
import com.ezrodriguez.Entities.Institutions;
import com.ezrodriguez.Entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class InstitutionsService {

    @Autowired
    private InstitutionsDao institutionsDao;


    public List<Institutions> getAllInstitutions(){

        return this.institutionsDao.getInstitutions();
    }

    public String Write(){

        return this.institutionsDao.writeExcel();
    }

    public String Read(){

        return this.institutionsDao.readExcel();
    }
}
