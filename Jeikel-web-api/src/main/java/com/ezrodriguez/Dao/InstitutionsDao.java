package com.ezrodriguez.Dao;

import com.ezrodriguez.Entities.Institutions;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import  org.apache.poi.*;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
@Repository
public class InstitutionsDao {

    private static final String FILE_NAME = "Prueba.xlsx";

    public List<Institutions> getInstitutions(){
        Connection con = getConnection();
        List<Institutions> Institutionss = new ArrayList<Institutions>() {
        };

        Institutions newInstitutions = new Institutions(
               1,
                "Prueba",
                "MI DESCRIPCION DE PRUEBA",
                 false,
                0 ,
                1
        );
        Institutionss.add(newInstitutions);

        return Institutionss;
    }


    public String writeExcel(){
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("My friends");
        Object[][] friends = {
                {"Nombre", "Titulo"},
                {"Diana", "monita"},
                {"Angel", "Ware"},
                {"Emmanuel", "gAY"},
                {"mANUEL", "pUTO"}
        };

        int rowNum = 0;
        System.out.println("Creating excel");

        for (Object[] friend : friends) {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : friend) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Done";
    }

    public  String readExcel(){
        try {

            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    //getCellTypeEnum shown as deprecated for version 3.15
                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + "--");
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + "--");
                    }

                }
                System.out.println();

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  "Done";
    }

    private Connection getConnection() {
        try{
            String connectionUrl = "jdbc:sqlserver://localhost:1433;database=JeikelDb;integratedSecurity=true;";
            Connection cnn = DriverManager.getConnection(connectionUrl);
            return cnn;
        }catch (SQLException ex){
            return null;
        }

    }

//    public Institutions getInstitutionsById(int id) {
//        Connection con = getConnection();
//        if (con == null) {
//            return null;
//        } else {
//            return getInstitutionsBy_Id(id);
//        }
//    }
//
//    public boolean createInstitutions(Institutions Institutions) {
//        Institutions InstitutionsToCheck;
//        InstitutionsToCheck = getInstitutionsByName(Institutions.getInstitutionsName());
//        if(InstitutionsToCheck != null){
//            return false;
//        }else{
//            String query = "INSERT INTO Institutionss VALUES ('"+Institutions.getInstitutionsName()+"','"+
//                    Institutions.getDescription()+"',"+
//                    Institutions.getQuantityStoraged()+","+
//                    Institutions.getPurchasePrice()+","+
//                    Institutions.getSalePrice()+")";
//            return executeUpdate(query);
//
//        }
//
//    }
//
//    public boolean updateInstitutions(Institutions Institutions){
//        Institutions InstitutionsToCheck;
//        InstitutionsToCheck = getInstitutionsById(Institutions.getId());
//        if(Institutions != InstitutionsToCheck){
//            String query = "UPDATE Institutionss SET Institutions_name = '"+Institutions.getInstitutionsName()+
//                    "', Institutions_description = '"+ Institutions.getDescription()+
//                    "', quantity_storaged ="+ Institutions.getQuantityStoraged()+
//                    ", purchase_price= "+ Institutions.getPurchasePrice()+
//                    ", sale_price="+Institutions.getSalePrice()+" WHERE id ="+ Institutions.getId();
//            return executeUpdate(query);
//        }else {
//            return true;
//        }
//    }
//
//    private boolean executeUpdate(String query){
//        try {
//            Connection con = getConnection();
//            Statement state;
//            state = con.createStatement();
//            int result = state.executeUpdate(query);
//
//            if(result ==1){
//                return true;
//            }
//            return false;
//        } catch (SQLException ex) {
//            return false;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    public TransferResponse entryInstitutionss(int id, long entry){
//        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
//        int year = Calendar.YEAR;
//        int month = Calendar.MONTH;
//        int day = Calendar.DAY_OF_MONTH;
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, year);
//        cal.set(Calendar.MONTH, month - 1); // <-- months start
//        cal.set(Calendar.DAY_OF_MONTH, day);
//        java.sql.Date date = new java.sql.Date(cal.getTimeInMillis());
//
//        Connection con = getConnection();
//        Institutions Institutions = getInstitutionsById(id);
//        Institutions.setQuantityStoraged(Institutions.getQuantityStoraged() + entry);
//        int resultUpdate, resultTransfer;
//        Statement state;
//        String query = "UPDATE Institutionss SET quantity_storaged = "+ Institutions.getQuantityStoraged() + " WHERE id ="+ id;
//        try {
//            state = con.createStatement();
//            resultUpdate = state.executeUpdate(query);
//        }catch (SQLException e){
//            return null;
//        }
//        if(resultUpdate == 1) {
//            Transfer transfer = new Transfer(Institutions.getId(), Date.from(Instant.now()), TransferType.EntryTransfer,
//                    entry, Institutions.getPurchasePrice());
//            String queryTransfer= "INSERT INTO TRANSFERS ([Institutions_id]" +
//                    "           ,[transfer_type]" +
//                    "           ,[quantity]" +
//                    "           ,[unit_cost]) VALUES("+ transfer.InstitutionsId+
//                    ","+ transfer.TransferType.ordinal()+","+
//                    transfer.Quantity+"," +  transfer.UnitCost+")";
//            try{
//                resultTransfer = state.executeUpdate(queryTransfer);
//            }catch (SQLException e){
//                return null;
//            }
//            if (resultTransfer == 1) {
//                return new TransferResponse(transfer);
//            }
//            else{
//                return null;
//            }
//        }else{
//            return null;
//        }
//    }
//
//    public TransferResponse outInstitutionss(int id, long out){
//        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
//        int year = Calendar.YEAR;
//        int month = Calendar.MONTH;
//        int day = Calendar.DAY_OF_MONTH;
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, year);
//        cal.set(Calendar.MONTH, month - 1); // <-- months start
//        cal.set(Calendar.DAY_OF_MONTH, day);
//        java.sql.Date date = new java.sql.Date(cal.getTimeInMillis());
//
//        Connection con = getConnection();
//        Institutions Institutions = getInstitutionsById(id);
//        Institutions.setQuantityStoraged(Institutions.getQuantityStoraged() - out);
//        long quantity = Institutions.getQuantityStoraged();
//        if(quantity - out <=0){
//            return new TransferResponse("QuantitySoldOut");
//        }
//        int resultUpdate, resultTransfer;
//        Statement state;
//        String query = "UPDATE Institutionss SET quantity_storaged = "+ Institutions.getQuantityStoraged() + " WHERE id ="+ id;
//        try {
//            state = con.createStatement();
//            resultUpdate = state.executeUpdate(query);
//        }catch (SQLException e){
//            return null;
//        }
//        if(resultUpdate == 1) {
//            Transfer transfer = new Transfer(Institutions.getId(), Date.from(Instant.now()), TransferType.OutTransfer,
//                    out, Institutions.getSalePrice());
//            String queryTransfer= "INSERT INTO TRANSFERS ([Institutions_id]" +
//                    "           ,[transfer_type]" +
//                    "           ,[quantity]" +
//                    "           ,[unit_cost]) VALUES("+ transfer.InstitutionsId+
//                    ","+ transfer.TransferType.ordinal() +","+
//                    transfer.Quantity+"," +  transfer.UnitCost+")";
//            try{
//                resultTransfer = state.executeUpdate(queryTransfer);
//            }catch (SQLException e){
//                return null;
//            }
//            if (resultTransfer == 1) {
//                return new TransferResponse(transfer);
//            }
//            else{
//                return null;
//            }
//        }else{
//            return null;
//        }
//    }
//
//    public String writeReport() {
//        String ruta = "reporte.txt";
//        File archivo = new File(ruta);
//        BufferedWriter bw;
//        try{
//            if(archivo.exists()) {
//                bw = new BufferedWriter(new FileWriter(archivo));
//                bw.write("El fichero de texto ya estaba creado.");
//            } else {
//                bw = new BufferedWriter(new FileWriter(archivo));
//                bw.write("Acabo de crear el fichero de texto.");
//            }
//            bw.close();
//            return "La accion fue un exito";
//        }catch (Exception ex){
//            return "Hubo un problema al ejecutar la accion" + ex.getMessage();
//        }
//    }
//
//
//
//    private Institutions getInstitutionsBy_Id(int id){
//        String query = "Select * from Institutionss WHERE id ="+ id;
//        return getInstitutions(query);
//    }
//
//    private Institutions getInstitutionsByName(String name){
//        String query = "Select * from Institutionss WHERE Institutions_name ="+ name;
//        return getInstitutions(query);
//    }
//
//    private Institutions getInstitutions(String query){
//        try {
//            Institutions Institutions = new Institutions();
//            Connection con = getConnection();
//            Statement state;
//            state = con.createStatement();
//            ResultSet rs = state.executeQuery(query);
//
//            while (rs.next()) {
//                Institutions newInstitutions = new Institutions(
//                        rs.getString("Institutions_name"),
//                        rs.getString("Institutions_description"),
//                        rs.getLong("quantity_storaged"),
//                        rs.getDouble("purchase_price"),
//                        rs.getDouble("sale_price")
//                );
//                newInstitutions.setId(rs.getInt("id"));
//                Institutions = newInstitutions;
//            }
//            return Institutions;
//        } catch (SQLException ex) {
//            return null;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//


}

